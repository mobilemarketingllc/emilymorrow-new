<?php
/*
Template Name: Blog 
*/
get_header(); ?>
<style>
body .fl-post-feed-meta
{
  display:none;
}
  </style>
<div class="site-content-with-sidebar blog">
  <h1 class="entry-title"><?php the_title(); ?></h1>
  <div id="content" class="site-content" role="main">
    <?php the_post(); ?>
    <?php the_content(); ?>
   <!--  <h2>Recent Posts</h2>
    <ul class="recent-posts">
      <?php /* $recent_posts = wp_get_recent_posts(array(
        'numberposts' => 5,
        'post_status' => 'publish'
      ));
      foreach ($recent_posts as $recent) { 
        $permalink = get_permalink($recent['ID']); ?>
      <li>
        <h3><a href="<?php echo $permalink; ?>"><?php echo $recent['post_title']; ?></a></h3>
        <?php echo get_the_excerpt($recent['ID']); ?>
        <p><a href="<?php echo $permalink; ?>">Read more</a></p>
      </li>
      <?php } */ ?>
    </ul> -->
  </div>  <!-- #content -->
  <div id="secondary" class="widget-area" role="complementary">
    <ul class="sidebar-widgets">
      <?php dynamic_sidebar('emh-blog-sidebar'); ?>
    </ul>
  </div><!-- #secondary -->
</div>
<?php get_footer(); ?>