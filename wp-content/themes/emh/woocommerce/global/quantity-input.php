<?php
/**
 * Product quantity inputs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/quantity-input.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="quantity">
	<label for="quantity">Quantity: </label>
	<input type="number" 
		step="<?php echo esc_attr( $step ); ?>" 
		min="<?php echo esc_attr( $min_value ); ?>" 
		max="<?php echo esc_attr( $max_value ); ?>" 
		name="<?php echo esc_attr( $input_name ); ?>" 
		value="<?php echo esc_attr( $input_value ); ?>" 
		title="<?php echo esc_attr_x( 'Qty', 'Product quantity', 'woocommerce' ) ?>" 
		class="input-text qty text" 
		size="4" 
		pattern="<?php echo esc_attr( $pattern ); ?>" 
		inputmode="<?php echo esc_attr( $inputmode ); ?>" />
    <?php
		if ($uom_plural !== '' && $uom_plural !== null) { ?>
		<span class="quantity-uom"><?php echo $uom_plural; ?></span>
	<?php }
		if (($min_value !== '' && strval($min_value) !== '0' && strval($min_value) !== '1') || ($max_value !== '')) {
			echo '<p class="quantity-note">(';
			$chunks = array();
			if ($min_value !== '' && strval($min_value) !== '0' && strval($min_value) !== '1') {
				$min_msg = 'minimum quantity ' . $min_value;
				if (strval($min_value) === '1' && $uom_singular !== '' && $uom_singular !== null) {
					$min_msg .= ' ' . $uom_singular;
				} else if (strval($min_value) !== '1' && $uom_plural !== '' && $uom_plural !== null) {
					$min_msg .= ' ' . $uom_plural;
				}
				$chunks[] = $min_msg;
			}
			if ($max_value !== '') {
				$max_msg = 'maximum quantity ' . $max_value;
                if (strval($max_value) === '1' && $uom_singular !== '' && $uom_singular !== null) {
					$max_msg .= ' ' . $uom_singular;
				} else if (strval($max_value) !== '1' && $uom_plural !== '' && $uom_plural !== null) {
					$max_msg .= ' ' . $uom_plural;
				}
				$chunks[] = $max_msg;
			}
			echo implode('; ', $chunks);
			echo ')</p>';
		}
    ?>
</div>
