<?php
/**
 * The template for displaying the Product Training.
 *
 * Template name: LAD Product Training
 *
 * @package storefront
 */
get_header();

?>
<style>
.label-text span
{
    color: #3F3F3F !important;
    font-size: 17.25px;
    font-weight: bold;
    line-height: 21px;
    text-align: center;
    display: block;
    padding: 5px;
}
</style>
    <link rel="stylesheet" href="<?php echo  bloginfo('stylesheet_directory'); ?>/assets/js/layout.css">
    <script type="text/javascript" src="<?php echo  bloginfo('stylesheet_directory'); ?>/assets/js/jquery.mixitup.min.js"></script>
    <script type="text/javascript" src="<?php echo  bloginfo('stylesheet_directory'); ?>/assets/js/html5lightbox/html5lightbox.js"></script>


    <div id="primary" class="content-area">
        <main id="main" class="site-main marketingMaterial productTraining" role="main">
         
            <div class="pageTitleBackBtn">
                <div class="backBtn"><a href="/lad-portal-home/">Back</a></div>
                <h1>ARCHITECTURAL SERIES PRODUCT TRAINING</h1>
            </div>
            
            <ul id="filters" class="clearfix">
                <?php 
                    $terms = get_terms( array(
                        'taxonomy' => 'lad_training_cat',
                        'hide_empty' => false, 
                        'orderby' => 'ID',
                        'order' => 'ASC',
                        ) ); 
                    foreach($terms as $term){
                ?>
                <li><span class="filter" data-filter=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></span></li>
                <?php } ?>
            </ul>
            <div id="portfoliolist">
                <?php 
                    $args = array( 'post_type' => 'lad_producttraining','posts_per_page'=> -1 );
                    $loop = new WP_Query( $args );
                    while ( $loop->have_posts() ) : $loop->the_post();				
                    $term_obj_list = get_the_terms( $post->ID, 'lad_training_cat' );
                    

                    while( have_rows('product_gallery',$post->ID) ): the_row(); 

                    $gallerytype = get_sub_field('product_gallery_type', $post->ID);
                    $image_url =  get_sub_field('upload_image', $post->ID);
                    $video_url =  get_sub_field('video_url', $post->ID);
                    $video_image_url =  get_sub_field('video_image', $post->ID);
                    $download_image_url =  get_sub_field('download_file_type', $post->ID);
                    $tile =  get_sub_field('image_title', $post->ID);
                    
	            ?>
                <div class="portfolio <?php echo $term_obj_list[0]->slug?>" data-cat="<?php echo $term_obj_list[0]->slug;?>">
                    <div class="portfolio-wrapper">

                    <?php if($gallerytype == 'video') {?>

                    <a href="<?php echo $video_url; ?>" data-showsocial="false" 
                    data-shareimage="<?php echo $video_image_url; ?>" data-thumbnail="<?php echo $video_image_url; ?>" 
                    class="html5lightbox video"  title="<?php echo $title; ?>">
                    <img src="<?php echo $video_image_url; ?>">
                    </a>

                    <?php }else{ ?>

                        <a href="<?php echo $image_url; ?>" data-showsocial="false" 
                        data-transition="crossfade" data-thumbnail="<?php echo $image_url; ?>" class="html5lightbox" 
                         data-width="600" data-height="400" >
                        <img src="<?php echo $image_url; ?>">
                        </a>

                    <?php } ?>
                    
                        <div class="label">
                            <div class="label-text">
                                 <span class="text-title" style="float:left;">
                                    <?php echo $tile; ?>
                                 </span>                            
                                    <a href="<?php echo $download_image_url; ?>" download> <img src="/wp-content/uploads/2018/12/download.png" width="24" /> </a>
                               
                            </div>
                            <div class="label-bg"></div>
                        </div>
                    </div>
                </div>
                <?php endwhile; ?>

                <?php endwhile; ?>

            </div>
        </main>
        <!-- #main -->
    </div>
    <!-- #primary -->

    <script type="text/javascript">
                jQuery(function() {
                    var filterList = {
                        init: function() {
                            // MixItUp plugin
                            // http://mixitup.io
                            jQuery('#portfoliolist').mixItUp({
                                selectors: {
                                    target: '.portfolio',
                                    filter: '.filter'
                                },
                                load: {
                                    filter: '.product-info'
                                }
                            });
                        }
                    };
                    // Run the show!
                    filterList.init();
                });
               

                jQuery(document).ready(function(){
                    jQuery('#html5-watermark').hide();
                });
            </script>

    <?php
get_footer();