<?php

// Flooring: Samples
add_action('cmb2_init', 'flooring_samples_metaboxes');
function flooring_samples_metaboxes() {
    $prefix = 'emh_';
    $cmb_group = new_cmb2_box(array(
        'id' => $prefix . 'samples',
        'title' => __('Samples and Add-Ons', 'storefront'),
        'object_types' => array('product'),
        'closed' => false,
        'show_on_cb' => 'cmb_is_flooring'
    ));

    $cmb_group->add_field(array(
        'id' => 'sample_box',
        'name' => __('Sample Box', 'storefront'),
        'type' => 'select',
        'options_cb' => 'get_sample_boxes'
    ));

    $cmb_group->add_field(array(
        'id' => 'sample_square',
        'name' => __('Sample Square', 'storefront'),
        'type' => 'select',
        'options_cb' => 'get_sample_squares'
    ));

    $cmb_group->add_field(array(
        'id' => 'molding',
        'name' => __('Molding', 'storefront'),
        'type' => 'select',
        'options_cb' => 'get_molding'
    ));
}

function cmb_is_flooring($cmb) {
    $terms = get_the_terms($cmb->object_id(), 'product_cat');
    if (!empty($terms)) {
        foreach ($terms as $term) {
            if ($term->slug === "flooring" || $term->slug === "architectural-series-category" ) {
                return true;
            }
        }
    }
    return false;
}

function get_sample_boxes() {
    return get_sample_products('flooring-boxes');
}

function get_sample_squares() {
    return get_sample_products('flooring-squares');
}

function get_molding() {
    return get_sample_products('molding');
}

function get_sample_products($product_cat) {
    $sample_products = array();
    $sample_products[0] = '- Select -';

    // WATCHOUT - At least as of 4.8.1 using WP_Query in here will break the global post
    // and cause other metaboxes/plugins to lose sight of the main post being edited.
    $sample_loop = get_posts(array(
        'post_type' => array('product'),
        'posts_per_page' => -1,
        'product_cat' => $product_cat,
        'orderby' => 'title',
        'order' => 'ASC'
    ));

    if (!empty($sample_loop)) {
        foreach ($sample_loop as $sample_post)
        {
            $sample_products[$sample_post->ID] = $sample_post->post_title;
        }
    }

    return $sample_products;
}
?>